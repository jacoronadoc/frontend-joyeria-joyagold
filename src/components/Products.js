import React, { useState, useEffect } from 'react';
import { DataGrid } from '@mui/x-data-grid';
import { Modal, Button } from 'react-bootstrap';
import "../styles/Products.css";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';

const Products = () => {
  const [rows, setRows] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [showModalEdit, setShowModalEdit] = useState(false);
  const [formData, setFormData] = useState({
    nombre: '',
    material: '',
    tipo: '',
    precio: '',
    color: '',
    peso: '',
    longitud: ''
  });

  const [formDataEdit, setFormDataEdit] = useState({
    nombre: '',
    material: '',
    tipo: '',
    precio: '',
    color: '',
    peso: '',
    longitud: ''
  });

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await fetch('http://localhost:3001/api/products/filter');
      const data = await response.json();
      setRows(data);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleInputChangeEdit = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formDataEdit, [name]: value });
  };

  const handleSubmit = async () => {
    try {
      const response = await fetch('http://localhost:3001/api/products', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      });
      if (response.ok) {
        setShowModal(false);
        fetchData();
      } else {
        console.error('Error al guardar el producto');
      }
    } catch (error) {
      console.error('Error al guardar el producto:', error);
    }
  };

  const handleSubmitEdit = async () => {
    
  };

  const handleEdit = (row) => {
    setFormDataEdit(row);
    setShowModalEdit(true);
  };

  const handleDelete = async (id) => {
    const confirmDelete = window.confirm('¿Estás seguro de que deseas eliminar este producto?');
    if (confirmDelete) {
      try {
        const response = await fetch(`http://localhost:3001/api/products/${id}`, {
          method: 'DELETE'
        });
        if (response.ok) {
          fetchData();
        } else {
          console.error('Error al eliminar el producto');
        }
      } catch (error) {
        console.error('Error al eliminar el producto:', error);
      }
    }
  };

  const columns = [
    { field: 'id', headerName: 'ID', flex: 0.1 },
    { field: 'nombre', headerName: 'Nombre', flex: 2 },
    { field: 'material', headerName: 'Material', flex: 0.8 },
    { field: 'tipo', headerName: 'Tipo', flex: 1 },
    { field: 'precio', headerName: 'Precio', flex: 1 },
    { field: 'color', headerName: 'Color', flex: 1 },
    { field: 'peso', headerName: 'peso', flex: 1 },
    { field: 'longitud', headerName: 'Longitud', flex: 1 },
    {
      field: 'actions',
      headerName: 'Acciones',
      flex: 1,
      renderCell: (params) => (
        <>
          <Button variant="primary" onClick={() => handleEdit(params.row)}>Editar <EditIcon /></Button>
          <Button variant="danger" onClick={() => handleDelete(params.row.id)}>Eliminar <DeleteIcon /></Button>
        </>
      )
    },
  ];

  return (
    <div className="page-container">
      <h1>PRUEBA técnica Jonathan Coronado</h1>
      <h2>Joyería JoyaGold</h2>
      <Button onClick={() => setShowModal(true)}>Crear producto</Button>
      
      <div className="table-container">
        <div className="my-destinations-container">
          <DataGrid
            rows={rows}
            columns={columns}
            pageSize={5}
            checkboxSelection
          />
        </div>
      </div>
      
      {/******************* MODAL DE CREACION ************************************************/}
      <Modal show={showModal} onHide={() => setShowModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Crear Producto</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div className="form-group">
              <label>Nombre</label>
              <input type="text" className="form-control" name="nombre" value={formData.nombre} onChange={handleInputChange} />
            </div>
            <div className="form-group">
              <label>Material</label>
              <select className="form-control" name="material" value={formData.material} onChange={handleInputChange}>
                <option value="oro">Oro</option>
                <option value="plata">Plata</option>
              </select>
            </div>
            <div className="form-group">
              <label>Tipo</label>
              <select className="form-control" name="tipo" value={formData.tipo} onChange={handleInputChange}>
                <option value="anillo">Anillo</option>
                <option value="cadena">Cadena</option>
                <option value="reloj">Reloj</option>
              </select>
            </div>
            <div className="form-group">
              <label>Precio</label>
              <input type="text" className="form-control" name="precio" value={formData.precio} onChange={handleInputChange} />
            </div>
            <div className="form-group">
              <label>Color</label>
              <input type="text" className="form-control" name="color" value={formData.color} onChange={handleInputChange} />
            </div>
            <div className="form-group">
              <label>Peso</label>
              <input type="text" className="form-control" name="peso" value={formData.peso} onChange={handleInputChange} />
            </div>
            <div className="form-group">
              <label>Longitud</label>
              <input type="text" className="form-control" name="longitud" value={formData.longitud} onChange={handleInputChange} />
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowModal(false)}>Cerrar</Button>
          <Button variant="primary" onClick={handleSubmit}>Guardar</Button>
        </Modal.Footer>
      </Modal>




      {/******************* MODAL DE EDICIÓN ************************************************/}
      <Modal show={showModalEdit} onHide={() => setShowModalEdit(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Crear Producto</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div className="form-group">
              <label>Nombre</label>
              <input type="text" className="form-control" name="nombre" value={formDataEdit.nombre} onChange={handleInputChangeEdit} readonly />
            </div>
            <div className="form-group">
              <label>Material</label>
              <select className="form-control" name="material" value={formDataEdit.material} onChange={handleInputChangeEdit} readonly>
                <option value="oro">Oro</option>
                <option value="plata">Plata</option>
              </select>
            </div>
            <div className="form-group">
              <label>Tipo</label>
              <select className="form-control" name="tipo" value={formDataEdit.tipo} onChange={handleInputChangeEdit} >
                <option value="anillo">Anillo</option>
                <option value="cadena">Cadena</option>
                <option value="reloj">Reloj</option>
              </select>
            </div>
            <div className="form-group">
              <label>Precio</label>
              <input type="text" className="form-control" name="precio" value={formDataEdit.precio} onChange={handleInputChangeEdit} />
            </div>
            <div className="form-group">
              <label>Color</label>
              <input type="text" className="form-control" name="color" value={formDataEdit.color} onChange={handleInputChangeEdit} readonly/>
            </div>
            <div className="form-group">
              <label>Peso</label>
              <input type="text" className="form-control" name="peso" value={formDataEdit.peso} onChange={handleInputChangeEdit} readonly/>
            </div>
            <div className="form-group">
              <label>Longitud</label>
              <input type="text" className="form-control" name="longitud" value={formDataEdit.longitud} onChange={handleInputChangeEdit} readonly/>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowModalEdit(false)}>Cerrar</Button>
          <Button variant="primary" onClick={handleSubmitEdit}>Guardar</Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default Products;
