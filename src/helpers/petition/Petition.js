export async function Petition(endpoint, options = {}) {
  try {
    let response = await fetch(`${process.env.REACT_APP_SERVICES}${endpoint}`, {
      method: options.method,
      headers: options.headers,
      body: options.body
    });
    if (response) {
      const data = await response.json();
      return data;
    } else {
      return {}
    }
  } catch (error) {
    return {}
  }
};
