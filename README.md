# Prueba tćnica front end - Joyería JoyaGold - Panel de Administración

Este proyecto es un panel de administración para gestionar los productos de la joyería JoyaGold. Permite crear, editar, ver detalles y eliminar productos de la base de datos.

## Características

- Creación de nuevos productos con información detallada.
- Edición de productos existentes.
- Visualización de detalles de cada producto.
- Eliminación segura de productos.
- Interfaz de usuario intuitiva y fácil de usar.
- Integración con backend para el almacenamiento y gestión de datos.

## Tecnologías Utilizadas

- React: Framework de JavaScript para construir interfaces de usuario.
- Material-UI: Biblioteca de componentes para React que implementa el diseño de Google Material Design.
- React Bootstrap: Biblioteca de componentes de Bootstrap para React.
- MUI Data Grid: Componente de tabla de datos para React.
- CSS: Estilos personalizados para mejorar la apariencia y la experiencia del usuario.

## Instalación

1. Clona este repositorio en tu máquina local.
2. Instala las dependencias utilizando el comando `npm install`.
3. Inicia el servidor de desarrollo con el comando `npm start`.

```bash
git clone ...
cd frontend-joyeria-joyagold
npm install
npm start